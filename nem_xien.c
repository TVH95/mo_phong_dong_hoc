#include<stdio.h>
#include<math.h>

#define g 9.8									// gia toc trong truong g=9.8 m/ss
#define theta0 (60*M_PI/180)				// goc nem ban dau 60 do

void update (double *r, double *v, double *a, double *h, double *r_moi, double *v_moi) {
	// thuat toan Verlet
	(*r_moi) = (*r) + (*v)*(*h) + 0.5*(*a)*(*h)*(*h);
	(*v_moi) = (*v) + (*a)*(*h);
}

int main() {
	double 	x[2], y[2],						// toa do
				vx[2], vy[2],					// van toc
				ax=0, ay=(-g);					// gia toc
	double 	h=0.001,						// delta t ( buoc nhay thoi gian)
				v0;								// van toc ban dau
	int 			i,
				n=1e3;							// so khung hinh ( n*h = tong thoi gian)
	printf("Van toc ban dau: "); scanf("%lf", &v0);
	x[0]=y[0]=0;								// toa do ban dau tai goc he toa do
	vx[0]=v0*cos(theta0); vy[0]=v0*sin(theta0);		//chieu vector van toc theo truc x,y
	FILE *f=fopen("data.txt","w");
	
	// bat dau phan chinh
	for(i=0; i<n; i++) {
		update (&x[0], &vx[0], &ax, &h, &x[1], &vx[1]);
		update (&y[0], &vy[0], &ay, &h, &y[1], &vy[1]);
		fprintf(f, "%lf\t%lf\t%lf\t%lf\n", x[1], y[1], vx[1], vy[1]);
		x[0]=x[1]; y[0]=y[1]; vx[0]=vx[1]; vy[0]=vy[1];
	}
	fflush(f); fclose(f);
}