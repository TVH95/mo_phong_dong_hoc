#include<stdio.h>

double k,m;

void update (double *r, double *v, double *a, double *h, double *r_moi, double *v_moi, double *a_moi) {
	// thuat toan Verlet
	(*r_moi) = (*r) + (*v)*(*h) + 0.5*(*a)*(*h)*(*h);
	(*a_moi) = (-k*(*r_moi))/m;				// a=F/m=(-kx)/m
	(*v_moi) = (*v) + 0.5*(*a)*(*h) + 0.5*(*a_moi)*(*h);
}

int main() {
	double 	x[2],							// toa do
				vx[2],							// van toc
				ax[2];							// gia toc
	double 	h=0.01;						// delta t ( buoc nhay thoi gian)
	int 			i,
				n=1e3;							// so khung hinh ( n*h = tong thoi gian)
	printf("Hang so co gian: k = "); scanf("%lf", &k);
	printf("Khoi luong vat: m = "); scanf("%lf", &m);
	printf("Toa do ban dau: "); scanf("%lf", &x[0]);
	printf("Van toc ban dau: "); scanf("%lf", &vx[0]);
	ax[0]=(-k*x[0])/m;
	FILE *f=fopen("loxo1chieu.txt","w");
	
	// bat dau phan chinh
	for(i=0; i<n; i++) {
		update (&x[0], &vx[0], &ax[0], &h, &x[1], &vx[1], &ax[1]);
		fprintf(f, "%lf\t%lf\t%lf\t%lf\n", (i+1)*h, x[1], vx[1], ax[1]);
		x[0]=x[1]; vx[0]=vx[1]; ax[0]=ax[1];
	}
	fflush(f); fclose(f);
}